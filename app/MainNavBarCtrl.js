'use strict';

(function() {

    function MainNavBarCtrl($location, $log) {
        this.sections = [{
            name: 'Home',
            link: '/'
        }, {
            name: 'Contact',
            link: '/contact'
        }, {
            name: 'Publish',
            link: '/publish'
        }];

        this.isSelected = function(section) {
            return $location.path() === section.link;
        };

    }

    angular.module('madaramApp').controller('MainNavBarCtrl', ['$location', '$log', MainNavBarCtrl]);

})();