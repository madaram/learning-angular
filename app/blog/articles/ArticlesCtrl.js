'use strict';
(function() {

    function ArticlesCtrl($route, $location, $log, $anchorScroll, page) {
        var ctrlScope = this;
        ctrlScope.articles = page.articles;
        ctrlScope.totalItems = page.totalArticles;
        ctrlScope.currentPage = page.pageNumber;
        ctrlScope.itemPerPage = page.itemPerPage;

        $location.hash('top');
        $anchorScroll();

        ctrlScope.pageChanged = function() {
            $log.log('Page changed to (ctrlScope.currentPage): ' + ctrlScope.currentPage);
            $location.path('blog/articles/' + ctrlScope.currentPage);
        };
    }

    angular.module('blog.controllers').controller('ArticlesCtrl', ['$route', '$location', '$log', '$anchorScroll',
        'page', ArticlesCtrl]);
})();