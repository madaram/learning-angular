'use strict';
(function() {
    function Article($resource, $log) {
        return $resource('api/articles/:articleId', {
            articleId: '@id'
        });
    }

    angular.module('blog.services').factory('Article', ['$resource', '$log', Article]);
})();