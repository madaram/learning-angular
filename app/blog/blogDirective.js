'use strict';

(function() {

    var blogDirectives = angular.module('blog.directives', []);

    blogDirectives.directive('myBlogSidebar', function() {
        return {
            restrict: 'E',
            templateUrl: 'blog/blog-sidebar.html'
        };
    });
})();