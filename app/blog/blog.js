'use strict';
(function() {
    angular.module('blog.services', []);
    angular.module('blog.controllers', []);
    angular.module('blog', ['blog.controllers', 'blog.services', 'blog.directives']);
})();