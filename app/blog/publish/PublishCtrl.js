'use strict';
(function() {

    angular.module('blog.controllers').controller('PublishCtrl', ['Article', '$log', '$uibModal', '$location', PublishCtrl]);

    function PublishCtrl(Article, $log, $uibModal, $location) {
        var ctrlScope = this;
        this.article = {};
        this.article.createDate = new Date();
        this.article.author = 'madaram';

        this.publish = function(articleForm) {
            $log.log(ctrlScope.article);

            if (articleForm.$valid) {
                this.open();
            }
        };

        // modal
        this.open = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'blog/publish/publishValidation.modal.html',
                controller: ['$uibModalInstance', function($uibModalInstance) {
                    this.article = ctrlScope.article;
                    this.validate = function() {
                        $log.info('validate :' + new Date());
                        $uibModalInstance.close('close');
                    }
                    this.dismiss = function() {
                        $uibModalInstance.dismiss();
                    };
                }],
                controllerAs: 'publishModalCtrl',
                size: 'lg'
            });

            modalInstance.result.then(function(logmessage) {
                $log.info('Modal is ' + logmessage);
                $log.log('submitting article');
                Article.save(ctrlScope.article);
                $location.path('blog/articles/1');
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        this.validate = function() {
            $log.info('validate :' + new Date());
        };


    };
})();