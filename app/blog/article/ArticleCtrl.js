'use strict';
(function() {

    function ArticleCtrl($log, $route, article) {
        var ctrlScope = this;
        $log.log(article);
        ctrlScope.article = article;
    }

    angular.module('blog.controllers').controller('ArticleCtrl', ['$log', '$route', 'article', ArticleCtrl]);
})();