'use strict';

(function() {

    angular.module('blog.directives').directive('myArticlePreview', ['$log', function($log) {
        return {
            restrict: 'E',
            scope: {
                article: '=model',
                nolink: '@'
            },
            link: function(scope, element, attrs) {
                if (scope.nolink === 'true') {
                    scope.nolink = true;
                } else {
                    scope.nolink = false;
                }
            },
            templateUrl: 'blog/preview/article-preview.html'
        };
    }]);

})();