'use strict';

(function() {
    angular.module('madaramApp').controller('ErrorCtrl', ['$location', '$log', 'errorService', errorCtrl]);

    function errorCtrl($location, $log, errorService) {
        this.error = errorService.getError();
    }

})();