'use strict';
/**
 * The is not used anymore, I do not delete it in order to have directive examples. This was supposed to be used like :
 *
 *       <fieldset class="form-group" my-show-errors>
 *
 * <label class="control-label" for="articleTitle">Article title</label>
 * <label class="control-label">{{articleForm | json}}</label>
 *
 * <div ng-messages="articleForm.articleTitle.$error">
 * <label class="control-label" ng-message="required">You did not enter a field</label>
 * </div>
 *
 * <input type="text" class="form-control" id="articleTitle" name="articleTitle"
 * placeholder="Title of my article"
 * ng-model="publishCtrl.article.title" required>
 *
 * <small class="text-muted">The title of the article will be used to generate the link</small>
 *
 * </fieldset>
 */
(function() {

    function myShowErrors($log) {
        return {
            restrict: 'A',
            require: '^form',
            // we have the form ctrl because of require: '^form'
            link: function(scope, el, attrs, formCtrl) {
                // find the text box element, which has the 'name' attribute
                $log.log("hello");
                $log.log(attrs);
                // find input element
                var inputEl = el[0].querySelector("[name]");
                if (inputEl !== null) {
                    $log.log(inputEl);
                    // convert the native text box element to an angular element
                    var inputNgEl = angular.element(inputEl);
                    // get the name on the text box so we know the property to check
                    // on the form controller
                    var inputName = inputNgEl.attr('name');
                    // only apply the has-error class after the user leaves the text box
                    inputNgEl.bind('blur', function() {
                        el.toggleClass('has-error', formCtrl[inputName].$invalid);
                    });
                }

                // only apply the has-error class on show-errors-check-validity
                scope.$on('show-errors-check-validity', function() {
                    $log.log("show-errors-check-validity received");
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });
            }
        };
    };

    angular.module('common.directives')
        /**
         * Search for input inside the element, if exist, apply has-error css class to the element when form input is
         * invalid.
         */
        .directive('myShowErrors', ['$log', myShowErrors]);
})();