'use strict';

(function() {

    function myFieldset($log) {
        return {
            restrict: 'E',
            require: ['^form', 'myFieldset'],
            transclude: true,
            scope: {},
            // we have the form ctrl because of require: '^form'
            link: function(scope, el, attrs, ctrl) {
                var formCtrl = ctrl[0];
                var dirCtrl = ctrl[1];
                var inputEl = el[0].querySelector("[name]");
                // convert the native text box element to an angular element
                var inputNgEl = angular.element(inputEl);
                // get the name on the text box so we know the property to check
                // on the form controller
                var inputName = inputNgEl.attr('name');

                dirCtrl.hasError = function() {
                    return formCtrl[inputName].shouldShowError;
                };

            },
            controller: function() {
            },
            controllerAs: 'fieldsetCtrl',
            template: '<fieldset class="form-group" ng-class="{\'has-error\': fieldsetCtrl.hasError()}">'
            + '<div ng-transclude></div>'
            + '</fieldset>'
        };
    };

    angular.module('common.directives')
        /**
         * Search for input inside the element, if exist, apply has-error css class to the element when form input is
         * invalid and untouched or form submitted. Also, add hasError boolean value to the form model
         * (formCtrl.inputName.hasError).
         */
        .directive('myFieldset', ['$log', myFieldset]);
})();
