'use strict';

/**
 * Not very usefull, but I keep it because it still is an example.
 */

(function() {

    var commonDirectives = angular.module('common.directives');

    commonDirectives.directive('myButton', ['$log', function($log) {
        return {
            restrict: 'E',
            scope: {
                text: '@text',
                event: '&'
            },
            template: '<button type="submit" class="btn btn-primary" ng-click="event();">{{text}}</button>',
            link: function(scope, el, attrs) {
            }
        };
    }]);
})();

//// great example here http://stackoverflow.com/questions/13032621/need-some-examples-of-binding-attributes-in-custom-angularjs-tags
//
//app.directive('mytag',function() {
//    return {
//        restrict: 'E',
//        template: '<div>' +
//        '<input ng-model="controltype"/>' +
//        '<button ng-click="controlfunc()">Parent Func</button>' +
//        '<p>{{controlval}}</p>' +
//        '</div>',
//        scope: {
//            /* make typeattribute="whatever" bind two-ways (=)
//             $scope.whatever from the parent to $scope.controltype
//             on this directive's scope */
//            controltype: '=typeattribute',
//            /* reference a function from the parent through
//             funcattribute="somefunc()" and stick it our
//             directive's scope in $scope.controlfunc */
//            controlfunc: '&funcattribute',
//            /* pass a string value into the directive */
//            controlval: '@valattribute'
//        },
//        controller: function($scope) {
//        }
//    };
//});
//
//<div ng-controller="ParentCtrl">
//    <!-- your directive -->
//<mytag typeattribute="parenttype" funcattribute="parentFn()" valattribute="Wee, I'm a value"></mytag>
//    <!-- write out your scope value -->
//{{parenttype}}
//</div>
//
//
//app.controller('ParentCtrl', function($scope){
//    $scope.parenttype = 'FOO';
//    $scope.parentFn = function() {
//        $scope.parenttype += '!!!!';
//    }
//});
