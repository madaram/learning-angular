'use strict';

(function() {
    /**
     * Should be split in multiple directive ?
     * @param $log
     * @returns {{restrict: string, require: string, link: link}}
     */
    function myFormWithShouldShowError($log) {
        return {
            restrict: 'A',
            require: 'form',
            link: function(scope, el, attrs, formCtrl) {

                var shouldShowErrorMessages = [];

                var inputElements = el[0].querySelectorAll("[name]");

                // shouldShowError for inputs
                angular.forEach(inputElements, function(value) {
                    // convert the native input element to an angular element
                    var inputNgEl = angular.element(value);
                    // get the name on the text box so we know the property to check
                    // on the form controller
                    var inputName = inputNgEl.attr('name');

                    formCtrl[inputName].shouldShowError = false;

                    var shouldShowErrorMessage = function() {
                        return formCtrl[inputName].$invalid && (formCtrl[inputName].$touched || formCtrl.$submitted);
                    };

                    shouldShowErrorMessages.push(shouldShowErrorMessage);

                    scope.$watch(shouldShowErrorMessage, function(newValue, oldValue, scope) {
                        formCtrl[inputName].shouldShowError = newValue;
                    });
                });

                // shouldShowError for the form
                var formHasErrorToShow = function() {
                    for (var i = 0, len = shouldShowErrorMessages.length; i < len; i++) {
                        if (shouldShowErrorMessages[i]()) {
                            return true;
                        }
                    }
                    return false;
                };

                formCtrl.shouldShowError = false;
                scope.$watch(formHasErrorToShow, function(newValue, oldValue, scope) {
                    formCtrl.shouldShowError = newValue;
                });

            }
        };
    };

    angular.module('common.directives').directive('myFormWithShouldShowError', ['$log', myFormWithShouldShowError]);
})();
