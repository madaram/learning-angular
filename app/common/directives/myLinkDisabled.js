'use strict';
(function() {
    angular.module('common.directives').directive('myLinkDisabled', ['$log', function($log) {
        return {
            restrict: 'A',
            scope: {
                disabled: '@myLinkDisabled'
            },
            link: function(scope, element, attrs) {
                element.bind('click', function(event) {
                    if (scope.disabled === 'true') {
                        event.preventDefault();
                    }
                });
            }
        };
    }]);
})();