'use strict';
(function() {
    var module = angular.module('common.interceptors');

    module.factory('errorService', [function() {
        var error = 404;
        return {
            getError: function() {
                return error;
            },
            setError: function(errorToSet) {
                error = errorToSet;
            }
        };
    }]);

    module.factory('myErrorInterceptor', ['$log', '$location', 'errorService', function($log, $location, errorService) {

        var myErrorInterceptor = {
            request: function(config) {
                return config;
            },
            responseError: function(rejection) {
                $log.log('rejection');
                $log.log(rejection);
                $location.path('error');
                errorService.setError(rejection.status);
                return rejection;
            }
        };

        return myErrorInterceptor;
    }]);
})();