'use strict';

(function() {

    var app = angular.module('madaramApp', [
        'common.directives',
        'common.interceptors',
        'blog',
        'ngSanitize',
        'ngResource',
        'ngMessages',
        'ngRoute',
        'ui.bootstrap']);

    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/contact', {
            templateUrl: 'contact/contact.html'
        }).when('/blog/article/:articleLink', {
            templateUrl: 'blog/article/article.html',
            controller: 'ArticleCtrl',
            controllerAs: 'articleCtrl',
            resolve: {
                article: ['Article', '$route', function(Article, $route) {
                    return Article.get({
                        articleId: $route.current.params.articleLink
                    }).$promise;
                }]
            }
        }).when('/blog/articles/:pageNumber?', {
            templateUrl: 'blog/articles/articles.html',
            controller: 'ArticlesCtrl',
            controllerAs: 'articlesCtrl',
            resolve: {
                page: ['Article', '$route', '$log', function(Article, $route, $log) {
                    $log.log('resolving Article.get with pageNumber : ' + $route.current.params.pageNumber);
                    return Article.get({
                        pageNumber: Number($route.current.params.pageNumber)
                    }).$promise;
                }]
            }
        }).when('/publish', {
            templateUrl: 'blog/publish/publish.html',
            controller: 'PublishCtrl',
            controllerAs: 'publishCtrl'
        }).when('/error', {
            templateUrl: 'error/error.html',
            controller: 'ErrorCtrl',
            controllerAs: 'errorCtrl'
        }).otherwise({
            redirectTo: '/blog/articles/1',
        })
    }]);

    app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('myErrorInterceptor');
    }]);

})();