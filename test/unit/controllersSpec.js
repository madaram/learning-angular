'use strict';

//TODO : delete this when real dev has started
describe('first dummy test', function () {


    beforeEach(module('madaramApp'));

    function helloWorld() {
        return "Hello world!";
    }

    describe("Hello world", function () {
        it("says hello", function () {
            expect(helloWorld()).toEqual("Hello world!");
        });
    });

});
