'use strict';

describe('my app', function () {

    it('should automatically redirect to /', function () {
        browser.get('index.html');
        expect(browser.getLocationAbsUrl()).toMatch("/");
    });

    describe('Contact page', function () {
        it('should redirect to contact when click on contact in navbar', function () {
            // when
            browser.get('index.html');
            element(by.id('contact')).click().then(function () {
                // then we are on page contact
                expect(browser.getLocationAbsUrl()).toMatch("/contact");
                expect(element(by.id('contact-header')).getText()).toContain("Hey everyone!");
            });
        });
    });

});
