var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser')
var app = express();

// log
app.use(morgan('combined'))

app.use(bodyParser.json())

// api for dev
var devApi = require('./fakes/devApi.js')
app.use('/app/api', devApi());
// static files
app.use('/app', express.static(__dirname + '/../app'));

app.listen(8080);
console.log("App listening on port 8080, try http://localhost:8080/app/");