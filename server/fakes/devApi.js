var express = require('express');
var router = express.Router();
var articles = require('./initial_articles.json');
var url = require('url');

module.exports = function() {
    // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
    router.get('/', function(req, res) {
        res.json({
            message: 'hey! welcome to our api!'
        });
    });

    // get articles paginated
    router.get('/articles', function(req, res) {
        var urlParts = url.parse(req.url, true);
        var query = urlParts.query;
        console.log(query);
        var itemPerPage = (typeof query.itemPerPage !== 'undefined' ? query.itemPerPage : 5);
        var pageNumber = (typeof query.pageNumber !== 'undefined' ? query.pageNumber : 1);

        var articlesToReturn = [];
        var i = 0;
        articles.forEach(function(article) {
            if (i >= (pageNumber - 1) * itemPerPage && i <= pageNumber * itemPerPage) {
                articlesToReturn.push(article);
            }
            i++;
        });
        var page = {
            articles: articlesToReturn,
            itemPerPage: itemPerPage,
            totalArticles: articles.length,
            pageNumber: Number(pageNumber)
        };
        res.json(page);
    });

    // get by id
    router.get('/articles/:articleId',
        function(req, res) {
            var articleId = req.params.articleId;
            var article = {};
            articles.forEach(function(item) {
                if (item.link === articleId) {
                    article = item;
                }
            });
            res.json(article);
        });

    function determineLink(name) {
        var link = name.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, '');
        link = link.replace(/ /g, '-');
        return link;
    }

    // post article
    router.post('/articles',
        function(req, res) {
            var article = req.body;
            article.link = determineLink(article.title);
            articles.unshift(article);
            res.send('OK');

        });


    return router;
}